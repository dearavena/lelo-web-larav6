<?php

namespace App\Http\Controllers\Masyarakat;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Models\Event;
use Carbon\Carbon;
use DataTables;
use Auth;
use PDF;
Use App\Model\Barang;
use App\User;
Use App\Model\Lelang;
use App\Model\HistoryLelang;
use App\Exports\LelangExport;

class LelangController extends Controller
{
    public function index(){
        $data_lelang = Lelang::with('barang','user')->where('status','=','open')->paginate(8); 
        return view('masyarakat.lelang.lelang',['data_lelang'=>$data_lelang]);
    }

    public function penawaran(Request $request){

        $validate = \Validator::make($request->all(), [
            'id_barang'  => 'required',
            'id_lelang'  => 'required',
            'penawaran_harga'  => 'required',
            ],

            $after_savee = [
                'alert' => 'danger',
                'icon' => 'attention',
                'title' => 'kesalahan !',
                'text-1' => 'Ini terjadi dalam menginput',
                'text-2' => 'Silakan coba lagi !'
            ]);

        if($validate->fails()){
            return redirect()->back()->with('after_savee', $after_savee);
        }

        if($request->penawaran_harga > $request->input('harga_awal_hidden')){

            $after_savee = [
                'alert' => 'success',
                'icon' => 'check',
                'title' => 'Berhasil ! ',
                'text-1' => 'Penawaran',
                'text-2' => 'Telah berhasil.'
            ];

            $data = new HistoryLelang();
            $data->id_lelang = $request->id_lelang;
            $data->id_barang = $request->id_barang;
            $data->id_user = Auth::user()->id_user;
            $data->penawaran_harga = $request->penawaran_harga;
            $data->created_at = Carbon::now();
            $data->save();

            $data = Lelang::where('id_lelang','=',$request->id_lelang)->first();
            $data->harga_akhir = $request->penawaran_harga;
            $data->save();
            
            return redirect()->back()->with('after_savee', $after_savee);

        }else{

            $after_savee = [
                'alert' => 'danger',
                'icon' => 'attention',
                'title' => 'kesalahan ! ',
                'text-1' => 'Penawaran',
                'text-2' => 'Harus diatas penawaran terakhir/harga awal .'
            ];
            return redirect()->back()->with('after_savee', $after_savee);
        }

    }

    public function cari (Request $request){

        $cari = $request->input('cari');

        $data_lelang = Lelang::with('barang','user')
        ->where('status','=','open')
        ->whereHas('barang', function ($query) use ($cari){
            $query->where('nama_barang', 'like',"%".$cari."%");
        })
        ->paginate(8); 
        return view('masyarakat.lelang.lelang',['data_lelang'=>$data_lelang]);
    }
}
