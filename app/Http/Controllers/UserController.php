<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use App\User;
use Carbon\Carbon;
use DataTables;
use Auth;
use App\Model\Notifikasi;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function ubahpassword(Request $request){

        $pass = $request->input('passwordlama');
        $newpass = $request->input('passwordbaru');
        $confirmpass = $request->input('passwordconfirm');
        
            if($newpass == $confirmpass){
                $user= Auth::user();
                $user->password = bcrypt($newpass);
                $user->save();

                $after_savee = [
                    'alert' => 'success',
                    'icon'  => 'check',
                    'title' => 'Berhasil !',
                    'text-1' => 'Password',
                    'text-2' => 'Berubah !'
                ];
         
                return redirect()->back()->with('after_savee', $after_savee);
         
              }else{
                $after_savee = [
                    'alert' => 'danger',
                    'icon'  => 'attention',
                    'title' => 'kesalahan !',
                    'text-1' => 'Konfirmasi ',
                    'text-2' => 'Password  !'
                ];
                  return redirect()->back()->with('after_savee', $after_savee);
            }
    }

    public function update_profile(Request $request){

        
        $after_savee = [
                  'alert' => 'success',
                  'icon' => 'check',
                  'title' => 'Berhasil ! ',
                  'text-1' => 'Data Profile',
                  'text-2' => 'telah Terupdate.'
              ];
  
              $id = $request->input('id_user');
              $profile= User::where('id_user', $id);
              $profile->update([
                'nama_lengkap' => $request->nama_lengkap,
                'username' => $request->username,
                'telp' => $request->telp,
  
              ]);
  
          return redirect()->back()->with('after_savee', $after_savee);
  
    }

    public function view_register(){

        return view('register');

    }

    public function register(Request $request){

        $validate = \Validator::make($request->all(), [
                'nama_lengkap' => 'required',
                'telp' => 'required',
                'username' => 'required',
                'password' => 'required',
                'hak_akses' => 'required'

                ],

                $after_save = [
                    'alert' => 'danger',
                    'icon' => 'attention',
                    'title' => 'kesalahan !',
                    'text-1' => 'Ini terjadi dalam menginput',
                    'text-2' => 'Silakan coba lagi !'
                ]);

            if($validate->fails()){
                return redirect()->back()->with('after_save', $after_save);
            }

            $after_save = [
                    'alert' => 'success',
                    'icon' => 'check',
                    'title' => 'Berhasil ! ',
                    'text-1' => 'Register',
                    'text-2' => 'Telah Berhasil.'
                ];


        $kosong = '';
        $now  = Carbon::now();

        $data = [
            'nama_lengkap' => $request->nama_lengkap,
            'telp' => $request->telp,
            'username' => $request->username,
            'password' => bcrypt($request->password),
            'is_admin_petugas' => $request->hak_akses,
            'is_petugas' => 0,
            'created_at'=> $now,
        ];

        $store = User::insert($data);

        return redirect()->back()->with('after_save', $after_save);

    }

    public function jsonnotif(){
        $data = Notifikasi::all();
        return Datatables::of($data)->make(true);
    }

}
