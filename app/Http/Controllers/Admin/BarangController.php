<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use DataTables;
use Auth;
use PDF;
Use App\Model\Barang;
Use App\Model\HistoryLelang;
Use App\Model\Lelang;
use Illuminate\Http\Request;


class BarangController extends Controller
{
    public function index(){
        
        return view('adminpetugas.barang.barang');
 
     }
 
     public function json(Request $request){
         $barang = Barang::All();
         return Datatables::of($barang)
         ->addColumn('action', function($barang){
                    return '
                    <a href="#" class="btn btn-small btn-default"  id="show-foto" style="float:left !important; margin-right:5"  data-toggle="modal" data-target="#showPhotos" ><i  class="fa fa-eye"></i></a>
                    <a href="#" class="btn btn-small btn-primary" id="show_edit" data-toggle="modal" data-target="#editForm" style="float:left !important; margin-right:5" ><i  class="fa fa-edit" ></i></a> 
                    <a href="delete_barang/'.$barang->id_barang.'" class="delete btn btn-small btn-danger"  class="delete" style="float:left !important; margin-right:5"><i  class="fa fa-trash"></i></a>';
         })
         ->make(true);
     }
 
     public function delete($id){
         $res_history = HistoryLelang::where('id_barang',$id)->delete();
         $res_lelang = Lelang::where('id_barang',$id)->delete();
         $res_barang= Barang::where('id_barang',$id)->delete();
         $after_save = [
                 'alert' => 'success',
                 'icon' => 'check',
                 'title' => 'Berhasil ! ',
                 'text-1' => 'Data Barang ',
                 'text-2' => 'Telah dihapus.'
             ];
         return redirect('/barang')->with('after_save', $after_save);
     
     }
 
     public function store(Request $request){
       
         $validate = \Validator::make($request->all(), [
             'kode_barang'  => 'required',
             'nama_barang'  => 'required',
             'foto_barang'  => 'required',
             'deskripsi'    => 'required',
             'harga_awal'   => 'required'
             ],
 
             $after_save = [
                 'alert' => 'danger',
                 'icon' => 'attention',
                 'title' => 'kesalahan !',
                 'text-1' => 'Ini terjadi dalam menginput',
                 'text-2' => 'Silakan coba lagi !'
             ]);
 
         if($validate->fails()){
             return redirect()->back()->with('after_save', $after_save);
         }
 
         $after_save = [
                   'alert' => 'success',
                   'icon' => 'check',
                   'title' => 'Berhasil ! ',
                   'text-1' => 'Data Barang ',
                   'text-2' => 'Telah tertambah.'
               ];

        $data = new Barang();
        $data->kode_barang = $request->kode_barang;
        $data->nama_barang = $request->nama_barang;
        $data->deskripsi = $request->deskripsi;
        $data->harga_awal = $request->harga_awal;
        $file = $request->foto_barang;
        $ext = $file->getClientOriginalExtension();
        $newName = rand(100000,1001238912).".".$ext;
        $file->move('uploads/file/barang',$newName);
        $data->foto_barang = $newName;
        $data->save();
        
        $data_post_barang = Barang::where('kode_barang','=',$request->kode_barang)->first();
        $data_lelang = new Lelang();
        $data_lelang->id_barang = $data_post_barang->id_barang;
        $data_lelang->status = 'nonactive';
        $data_lelang->tgl_lelang = null;
        $data_lelang->save();
 
        return redirect('/barang')->with('after_save', $after_save);
 
     }
 
     public function update(Request $request){
 
        $validate = \Validator::make($request->all(), [
            'kode_barang'  => 'required',
            'nama_barang'  => 'required',
            'deskripsi'    => 'required',
            'harga_awal'   => 'required'
            ],

            $after_save = [
                'alert' => 'danger',
                'icon' => 'attention',
                'title' => 'kesalahan !',
                'text-1' => 'Ini terjadi dalam menginput',
                'text-2' => 'Silakan coba lagi !'
            ]);

        if($validate->fails()){
            return redirect()->back()->with('after_save', $after_save);
        }

        $after_save = [
                  'alert' => 'success',
                  'icon' => 'check',
                  'title' => 'Berhasil ! ',
                  'text-1' => 'Data Barang ',
                  'text-2' => 'Telah diubah.'
              ];

       $id = $request->input('id_barang');
       
       $data = Barang::where('id_barang','=',$id)->first();
       $data->kode_barang = $request->kode_barang;
       $data->nama_barang = $request->nama_barang;
       $data->deskripsi = $request->deskripsi;
       $data->harga_awal = $request->harga_awal;
       if (empty($request->foto_barang)){
        $data->foto_barang =  $data->foto_barang;
       }
       else{
        unlink('uploads/file/barang/'.$data->foto_barang);
        $file = $request->foto_barang;
        $ext = $file->getClientOriginalExtension();
        $newName = rand(100000,1001238912).".".$ext;
        $file->move('uploads/file/barang',$newName);
        $data->foto_barang = $newName;
       }
       $data->save();

       return redirect('/barang')->with('after_save', $after_save);
 
     }
 
     public function import(Request $request){
         // validasi        
 
         $valid = Validator::make($request->all(), [
             'file'      => 'required|mimes:xls,xlsx'
         ])->setAttributeNames([
             'file'      => 'Data',
         ]);
         
         $after_save = [
             'alert' => 'danger',
             'icon' => 'attention',
             'title' => 'kesalahan !',
             'text-1' => 'File yang diizinkan hanya xls, xlsx',
             'text-2' => 'Silakan coba lagi !'
         ];
 
         if ($valid->fails()) {
             return redirect()->back()->with('after_save', $after_save);
         }
  
         // menangkap file excel
         $file = $request->file('file');
  
         // membuat nama file unik
         $nama_file = rand().$file->getClientOriginalName();
  
         // upload ke folder file_siswa di dalam folder public
         $file->move('tamu_gambaran_import',$nama_file);
  
         // import data
         Excel::import(new TamuGambaranImport, public_path('/tamu_gambaran_import/'.$nama_file));
 
         $after_save = [
             'alert' => 'success',
             'icon' => 'check',
             'title' => 'Berhasil ! ',
             'text-1' => 'Import ',
             'text-2' => 'Telah berhasil.'
         ];
 
         return redirect('/tamu_gambaran')->with('after_save', $after_save);
 
     }
}
