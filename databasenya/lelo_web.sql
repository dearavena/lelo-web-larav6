-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Waktu pembuatan: 13 Apr 2020 pada 16.31
-- Versi server: 5.7.26
-- Versi PHP: 7.3.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `lelo_web`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2020_04_04_181640_create_tbl_barang_table', 1),
(4, '2020_04_04_182111_create_tbl_lelang_table', 1),
(5, '2020_04_04_182747_create_tbl_history_lelang_table', 1),
(6, '2020_04_13_000913_create_tbl_notifikasi_table', 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_barang`
--

CREATE TABLE `tbl_barang` (
  `id_barang` int(10) UNSIGNED NOT NULL,
  `kode_barang` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foto_barang` text COLLATE utf8mb4_unicode_ci,
  `nama_barang` text COLLATE utf8mb4_unicode_ci,
  `deskripsi` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `harga_awal` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `tbl_barang`
--

INSERT INTO `tbl_barang` (`id_barang`, `kode_barang`, `foto_barang`, `nama_barang`, `deskripsi`, `harga_awal`, `created_at`, `updated_at`) VALUES
(8, '905596741', '973403500.jpg', 'Rumah Special 4 Type', 'Rumah tipe 36 adalah rumah yang mempunyai luas bangunan 36 m2. Contohnya adalah sebuah rumah dengan ukuran 6m x 6m = 36 m2', 130000000, '2020-04-12 14:53:25', '2020-04-12 14:53:25'),
(9, '329094233', '919771014.jpg', 'Motor Xtrill Special', 'Motor ini didesain dengan kecepatan penuh dan dapat melewati tikungan tajam dimanapun dan kapanpun.', 30000000, '2020-04-13 15:27:27', '2020-04-13 15:28:13');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_history_lelang`
--

CREATE TABLE `tbl_history_lelang` (
  `id_history` int(10) UNSIGNED NOT NULL,
  `id_lelang` int(11) DEFAULT NULL,
  `id_barang` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `penawaran_harga` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `tbl_history_lelang`
--

INSERT INTO `tbl_history_lelang` (`id_history`, `id_lelang`, `id_barang`, `id_user`, `penawaran_harga`, `created_at`, `updated_at`) VALUES
(11, 5, 8, 3, 132000000, '2020-04-12 16:58:20', '2020-04-12 16:58:20'),
(12, 6, 9, 3, 31000000, '2020-04-13 15:39:29', '2020-04-13 15:39:29');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_lelang`
--

CREATE TABLE `tbl_lelang` (
  `id_lelang` int(10) UNSIGNED NOT NULL,
  `id_user` int(11) DEFAULT NULL,
  `id_barang` int(11) NOT NULL,
  `id_petugas` int(11) DEFAULT NULL,
  `tgl_lelang` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `harga_akhir` int(11) DEFAULT NULL,
  `status` enum('nonactive','open','close') COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `tbl_lelang`
--

INSERT INTO `tbl_lelang` (`id_lelang`, `id_user`, `id_barang`, `id_petugas`, `tgl_lelang`, `harga_akhir`, `status`) VALUES
(5, 3, 8, 2, '2020-04-12 17:25:29', 132000000, 'close'),
(6, 3, 9, 2, '2020-04-13 15:49:31', 31000000, 'close');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_notifikasi`
--

CREATE TABLE `tbl_notifikasi` (
  `id_notif` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `isi_notif` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `tbl_notifikasi`
--

INSERT INTO `tbl_notifikasi` (`id_notif`, `title`, `isi_notif`, `created_at`, `updated_at`) VALUES
(1, 'Pemberitahuan Pemenang LelangRumah Special 4 Type', 'Dengan ditutupnya pelelangan ini, dengan barang yang dilelang adalah Rumah Special 4 Type dengan kode detail 905596741 telah dimenangkan oleh Masyarakat123 untuk informasi lanjut, mohon tunggu beberapa hari kedepan. Admin lelo akan menghubungi informasi kontak anda.', '2020-04-12 17:25:29', '2020-04-12 17:25:29'),
(2, 'Pemberitahuan Pemenang LelangMotor Xtrill Special', 'Dengan ditutupnya pelelangan ini, dengan barang yang dilelang adalah (Motor Xtrill Special) dengan kode detail (329094233) telah dimenangkan oleh (Masyarakat123) untuk informasi lanjut, mohon tunggu beberapa hari kedepan.Karena Admin LELO akan segera menghubungi kontak anda.', '2020-04-13 15:49:31', '2020-04-13 15:49:31');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id_user` int(10) UNSIGNED NOT NULL,
  `nama_lengkap` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telp` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_admin_petugas` tinyint(1) NOT NULL DEFAULT '0',
  `is_petugas` tinyint(1) NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id_user`, `nama_lengkap`, `username`, `password`, `telp`, `is_admin_petugas`, `is_petugas`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Administrator', 'admin123', '$2y$10$8JRgvND7XM9Hnyl18w.iQ.jkM5MJpwXXuPKTOcYGPd.icJAVIgfQ2', '08881331853', 1, 0, NULL, '2020-04-04 18:57:04', '2020-04-12 08:34:16'),
(2, 'Petugas Lelang', 'petugas123', '$2y$10$KPwuUccSRbAI5oCAVoxrbuf2qxj18I8oc9ZmKG/8QQpMSy4XOViK.', '088146338138', 1, 1, NULL, '2020-04-04 18:57:04', NULL),
(3, 'Masyarakat123', 'masyarakat123', '$2y$10$vxr6uz9m4sCo4DGzXC5k1uUKmQcNbLABEBacIopugPrk6/e6BmLau', '0883114472284', 0, 0, NULL, '2020-04-04 18:57:04', '2020-04-12 14:03:13'),
(4, 'Alfi Nur Hakim', 'alfinrhkm', '$2y$10$OcxyANkeK.4Mpy8yiwHNke6/feSkXT8o8ddVcR1jV/5LR7Q./GWB.', '09181919331', 0, 0, NULL, NULL, NULL),
(5, 'mas123', 'mas123', '$2y$10$XNVtU3a7gRSiXPGeTO31mODZhB1Sd4HVto1N/7X1agNNrepU98Tcq', '028718911', 0, 0, NULL, '2020-04-12 09:28:43', NULL);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indeks untuk tabel `tbl_barang`
--
ALTER TABLE `tbl_barang`
  ADD PRIMARY KEY (`id_barang`);

--
-- Indeks untuk tabel `tbl_history_lelang`
--
ALTER TABLE `tbl_history_lelang`
  ADD PRIMARY KEY (`id_history`);

--
-- Indeks untuk tabel `tbl_lelang`
--
ALTER TABLE `tbl_lelang`
  ADD PRIMARY KEY (`id_lelang`),
  ADD KEY `id_barang` (`id_barang`),
  ADD KEY `id_user` (`id_user`),
  ADD KEY `id_petugas` (`id_petugas`);

--
-- Indeks untuk tabel `tbl_notifikasi`
--
ALTER TABLE `tbl_notifikasi`
  ADD PRIMARY KEY (`id_notif`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `tbl_barang`
--
ALTER TABLE `tbl_barang`
  MODIFY `id_barang` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT untuk tabel `tbl_history_lelang`
--
ALTER TABLE `tbl_history_lelang`
  MODIFY `id_history` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT untuk tabel `tbl_lelang`
--
ALTER TABLE `tbl_lelang`
  MODIFY `id_lelang` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `tbl_notifikasi`
--
ALTER TABLE `tbl_notifikasi`
  MODIFY `id_notif` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id_user` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
