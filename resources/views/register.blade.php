<!DOCTYPE html>
<html lang="en">
<head>
	<title>LELO</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="{{ asset('assets') }}/login_/images/icons/price-tags.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ asset('assets') }}/login_/vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ asset('assets') }}/login_/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ asset('assets') }}/login_/vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="{{ asset('assets') }}/login_/vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ asset('assets') }}/login_/vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ asset('assets') }}/login_/css/util.css">
	<link rel="stylesheet" type="text/css" href="{{ asset('assets') }}/login_/css/main.css">
<!--===============================================================================================-->
<style>
    @import url('https://fonts.googleapis.com/css?family=Roboto+Slab:100,300,400,700');
    @import url('https://fonts.googleapis.com/css?family=Raleway:300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i');
</style>
<style>
    /* .wrap-login100 {
    width: 960px;
    background: #fff;
    border-radius: 10px;
    overflow: hidden;
    display: -webkit-box;
    display: -webkit-flex;
    display: -moz-box;
    display: -ms-flexbox;
    display: flex;
    flex-wrap: wrap;
    justify-content: space-between;
    padding: 100px 130px 33px 95px;
} */
</style>
</head>
<body>
	
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
                @if (Session::has('after_save'))
                <div class="col-md-12" style="margin-top:20px;">
                    <div class="alert alert-xs alert-dismissible alert-{{ Session::get('after_save.alert') }}">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ Session::get('after_save.title') }}</strong>
                        <a href="javascript:void(0)" class="alert-link">{{ Session::get('after_save.text-1') }}</a> {{ Session::get('after_save.text-2') }}
                    </div>
                </div>
                @endif 
				<div class="login100-pic js-tilt" data-tilt >
					<img src="{{ asset('assets') }}/login_/images/price-tags.png" alt="IMG">
				</div>

				<form class="login100-form validate-form" action="{{ URL('/post_register') }}" method="post" autocomplete="off">
            {{ csrf_field() }}
            
          <span class="login100-form-title">Aplikasi Lelang Online</span>
          @include('template.feedback')
          
					<div class="wrap-input100 validate-input" >
						<input class="input100" type="text" name="nama_lengkap" placeholder="Nama Lengkap" required>
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-user" aria-hidden="true"></i>
                        </span>
					</div>

					<div class="wrap-input100 validate-input" >
						<input class="input100" type="text" name="telp" placeholder="No Telepon" required>
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-user" aria-hidden="true"></i>
                        </span>
                    </div>
                    <br>
                    <div >
                        &nbsp;
                        <span>
                            <input type="radio" name="hak_akses" value='1'/>
                            <span style="color:#9a9a9a;">&nbsp;Admin</span>
                        </span>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <span>
                            <input type="radio" name="hak_akses" value='0' />
                            <span style="color:#9a9a9a;">&nbsp;Masyarakat</span>
                        </span>
                    </div>
                    <br>
                    <div class="wrap-input100 validate-input" data-validate = "Valid username is required">
						<input class="input100" type="text" name="username" placeholder="Username">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-user" aria-hidden="true"></i>
                        </span>
					</div>

					<div class="wrap-input100 validate-input" data-validate = "Password is required">
						<input class="input100" type="password" name="password" placeholder="Password">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-lock" aria-hidden="true"></i>
                        </span>
					</div>
					
					<div class="container-login100-form-btn">
						<button type="submit" class="login100-form-btn">
							Register
						</button>
					</div>

					<div class="text-center p-t-12">
						<span class="txt1">
							Anda sudah punya akun ? 
						</span>
						<a class="txt2" href="{{url('/')}}">
							Login
						</a>
					</div> 

					<div class="text-center p-t-136">
						<a class="txt2" href="#">
							Copyright By @Dearavena
						</a>
					</div>
				</form>
			</div>
		</div>
	</div>
	
	

	
<!--===============================================================================================-->	
	<script src="{{ asset('assets') }}/login_/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="{{ asset('assets') }}/login_/vendor/bootstrap/js/popper.js"></script>
	<script src="{{ asset('assets') }}/login_/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="{{ asset('assets') }}/login_/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="{{ asset('assets') }}/login_/vendor/tilt/tilt.jquery.min.js"></script>
	<script >
		$('.js-tilt').tilt({
			scale: 1.1
		})
	</script>
<!--===============================================================================================-->
	<script src="{{ asset('assets') }}/login_/js/main.js"></script>

</body>
</html>