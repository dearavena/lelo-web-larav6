<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class HistoryLelang extends Model
{
    protected $table='tbl_history_lelang';

    protected $primaryKey = 'id_history';

    public $timestamps = true;

    public $incrementing = true;

    protected $fillable = [
    'id_history','id_lelang','id_barang','id_user',
    'penawaran_harga','created_at'
    ];


    public function lelang()
    {
        return $this->belongsTo('App\Model\Lelang', 'id_lelang');
    }

    public function barang()
    {
        return $this->belongsTo('App\Model\Barang', 'id_barang');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'id_user');
    }
}
