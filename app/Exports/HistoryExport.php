<?php

namespace App\Exports;

use App\Model\HistoryLelang;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\View\View;
use App\Model\Barang;
use App\User;
Use App\Model\Lelang;

class HistoryExport implements FromView, ShouldAutoSize
{

    use Exportable;

    public function __construct(int $id_lelang)
    {
        $this->id_lelang = $id_lelang;
    }


    public function view(): View
    {
        $data = HistoryLelang::with('barang','user')->where('id_lelang','=',$this->id_lelang)
                ->orderBy('penawaran_harga','DESC')
                ->get(); 
        $nama_barang = null;
        foreach($data as $datanew){
            $nama_barang = $datanew->barang->nama_barang;
        }
        return view('adminpetugas.laporan.HistoryLelang', [
            'data'=>$data,
            'nama_barang'=>$nama_barang
        ]);
    }
}
