<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Models\Event;
use Carbon\Carbon;
use DataTables;
use Auth;
use PDF;
use App\Model\Barang;
use App\User;
Use App\Model\Lelang;
use App\Model\HistoryLelang;
use Excel;
use App\Exports\HistoryExport;
use Illuminate\Http\Request;

class HistoryController extends Controller
{
    public function index($id_lelang){
        
        $data_lelang = Lelang::with('barang','user')->where('id_lelang','=',$id_lelang)->get(); 
        $nama_barang = null;
        foreach($data_lelang as $datanew){
            $nama_barang = $datanew->barang->nama_barang;
        }
        return view('adminpetugas.history.history',['id_lelang'=>$id_lelang, 'nama_barang'=>$nama_barang]);
 
    }
    public function json($id_lelang){
        if($id_lelang == 'all'){
            $data = HistoryLelang::with('barang','user')->orderBy('created_at','DESC')->get();
        }else{  
            $data = HistoryLelang::with('barang','user')->where('id_lelang','=',$id_lelang)->orderBy('penawaran_harga','DESC')->get(); 
        }
        return Datatables::of($data)
        ->addColumn('action', function($data){
                return '
                <a href="/delete_history/'.$data->id_history.'" class="delete btn btn-small btn-danger"  class="delete" style="float:left !important; margin-right:5"><i  class="fa fa-trash"></i></a>
                ';
        })
        ->make(true);
    }

    public function delete($id){
        $res= HistoryLelang::where('id_history',$id)->delete();
        $after_save = [
                'alert' => 'success',
                'icon' => 'check',
                'title' => 'Berhasil ! ',
                'text-1' => 'Data History Lelang ',
                'text-2' => 'Telah dihapus.'
            ];
        return redirect()->back()->with('after_save', $after_save);
    
    }
    
    public function export(Request $request){

        $id_lelang = $request->get('id_lelang');
        $nama_barang = $request->get('nama_barang');
        if($id_lelang == null || $id_lelang == 0 || $id_lelang == ''){
            $after_save = [
                'alert' => 'danger',
                'icon' => 'attention',
                'title' => 'kesalahan !',
                'text-1' => 'Tidak ada data id yang dipilih / kosong',
                'text-2' => 'Silakan coba lagi !'
            ];

            return redirect()->back()->with('after_save', $after_save);
        }

        $data = HistoryLelang::with('barang','user')->where('id_lelang','=',$id_lelang)->get(); 
        $rand = rand ( 10000 , 99999 );
        $name_file = 'LaporanHistory '.$nama_barang.' '.$rand.' .xlsx';
        return (new HistoryExport($id_lelang))->download($name_file);
      }     
}
    