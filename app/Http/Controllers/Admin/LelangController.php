<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Models\Event;
use Carbon\Carbon;
use DataTables;
use Auth;
use PDF;
Use App\Model\Barang;
use App\User;
use App\Model\Notifikasi;
use App\Model\Lelang;
use App\Model\HistoryLelang;
use App\Exports\LelangExport;
use Illuminate\Http\Request;


class LelangController extends Controller
{
    public function index(){
        
        return view('adminpetugas.lelang.lelang');
 
    }
 
    public function json(Request $request){
        $data = Lelang::with('barang','user')->get(); 
        return Datatables::of($data)
        ->addColumn('action', function($data){
            if(Auth::user()->is_petugas == false){
                return '
                <a href="#" class="btn btn-md btn-default"  id="show-foto" style="float:left !important; margin-right:5"  data-toggle="modal" data-target="#showPhotos" ><i  class="fa fa-eye"></i></a>
                <a href="history/'.$data->id_lelang.'" class="btn btn-md btn-primary" style="float:left !important; margin-right:5" ><i  class="fa fa-history" ></i></a>
                ';
            }else{
                if($data->status == "nonactive"){
                    return '
                    <a href="#" class="btn btn-md btn-default"  id="show-foto" style="float:left !important; margin-right:5"  data-toggle="modal" data-target="#showPhotos" ><i  class="fa fa-eye"></i></a>
                    <a href="history/'.$data->id_lelang.'" class="btn btn-md btn-primary" style="float:left !important; margin-right:5" ><i  class="fa fa-history" ></i></a>
                    <a href="update_status_lelang/open/'.$data->id_lelang.'" class="btn btn-md btn-success"  style="float:left !important; margin-right:5; height: 28;
                    padding-top: 3;" >Open</a>
                    ';
                }else if($data->status == "open"){
                    return '
                    <a href="#" class="btn btn-md btn-default"  id="show-foto" style="float:left !important; margin-right:5"  data-toggle="modal" data-target="#showPhotos" ><i  class="fa fa-eye"></i></a>
                    <a href="history/'.$data->id_lelang.'" class="btn btn-md btn-primary" style="float:left !important; margin-right:5" ><i  class="fa fa-history" ></i></a>
                    <a href="update_status_lelang/close/'.$data->id_lelang.'" class="btn btn-md btn-danger" style="float:left !important; margin-right:5; height: 28;
                    padding-top: 3;" >Close</a>
                    ';
                }else{
                    return '
                    <a href="#" class="btn btn-md btn-default"  id="show-foto" style="float:left !important; margin-right:5"  data-toggle="modal" data-target="#showPhotos" ><i  class="fa fa-eye"></i></a>
                    <a href="history/'.$data->id_lelang.'" class="btn btn-md btn-primary" style="float:left !important; margin-right:5" ><i  class="fa fa-history" ></i></a>';
                }
            }
        })
        ->make(true);
    }
 
    public function delete($id){
        $res= Barang::where('id_barang',$id)->delete();
        $after_save = [
                'alert' => 'success',
                'icon' => 'check',
                'title' => 'Berhasil ! ',
                'text-1' => 'Data Barang ',
                'text-2' => 'Telah dihapus.'
            ];
        return redirect('/barang')->with('after_save', $after_save);
    
    }

    public function store(Request $request){
    
        $validate = \Validator::make($request->all(), [
            'kode_barang'  => 'required',
            'nama_barang'  => 'required',
            'foto_barang'  => 'required',
            'deskripsi'    => 'required',
            'harga_awal'   => 'required'
            ],

            $after_save = [
                'alert' => 'danger',
                'icon' => 'attention',
                'title' => 'kesalahan !',
                'text-1' => 'Ini terjadi dalam menginput',
                'text-2' => 'Silakan coba lagi !'
            ]);

        if($validate->fails()){
            return redirect()->back()->with('after_save', $after_save);
        }

        $after_save = [
                'alert' => 'success',
                'icon' => 'check',
                'title' => 'Berhasil ! ',
                'text-1' => 'Data Barang ',
                'text-2' => 'Telah tertambah.'
            ];

        $data = new Barang();
        $data->kode_barang = $request->kode_barang;
        $data->nama_barang = $request->nama_barang;
        $data->deskripsi = $request->deskripsi;
        $data->harga_awal = $request->harga_awal;
        $file = $request->foto_barang;
        $ext = $file->getClientOriginalExtension();
        $newName = rand(100000,1001238912).".".$ext;
        $file->move('uploads/file/barang',$newName);
        $data->foto_barang = $newName;
        $data->save();

        return redirect('/barang')->with('after_save', $after_save);

    }

    public function update_status($tipe, $id_lelang){

        $data = Lelang::where('id_lelang','=',$id_lelang)->first();
        if($tipe == 'open'){
            $data->tgl_lelang = Carbon::now();
            $data->id_petugas = Auth::user()->id_user;
        }else{
            $data_history = HistoryLelang::with('barang','user')
                            ->where('id_lelang','=',$id_lelang)
                            ->where('penawaran_harga','=',$data->harga_akhir)
                            ->orderBy('created_at','DESC')->first();

            $data_notif = New Notifikasi();
            $data_notif->title = 'Pemberitahuan Pemenang Lelang'.$data_history->barang->nama_barang;
            $data_notif->isi_notif = 'Dengan ditutupnya pelelangan ini, dengan barang yang dilelang adalah ('. $data_history->barang->nama_barang. ') dengan kode detail ('.$data_history->barang->kode_barang.') telah dimenangkan oleh ('.$data_history->user->nama_lengkap.') untuk informasi lanjut, mohon tunggu beberapa hari kedepan.Karena Admin LELO akan segera menghubungi kontak anda.';
            $data_notif->save();

            $data->id_user = $data_history->user->id_user;
        }
        $data->status = $tipe;
        $data->save();

        $after_save = [
            'alert' => 'success',
            'icon' => 'check',
            'title' => 'Berhasil ! ',
            'text-1' => 'Status',
            'text-2' => 'Telah diubah.'
        ];
        return redirect('/lelang')->with('after_save', $after_save);

    }
 
    
 
    public function import(Request $request){
        // validasi        
        $valid = Validator::make($request->all(), [
            'file'      => 'required|mimes:xls,xlsx'
        ])->setAttributeNames([
            'file'      => 'Data',
        ]);
        
        $after_save = [
            'alert' => 'danger',
            'icon' => 'attention',
            'title' => 'kesalahan !',
            'text-1' => 'File yang diizinkan hanya xls, xlsx',
            'text-2' => 'Silakan coba lagi !'
        ];

        if ($valid->fails()) {
            return redirect()->back()->with('after_save', $after_save);
        }

        // menangkap file excel
        $file = $request->file('file');

        // membuat nama file unik
        $nama_file = rand().$file->getClientOriginalName();

        // upload ke folder file_siswa di dalam folder public
        $file->move('tamu_gambaran_import',$nama_file);

        // import data
        Excel::import(new TamuGambaranImport, public_path('/tamu_gambaran_import/'.$nama_file));

        $after_save = [
            'alert' => 'success',
            'icon' => 'check',
            'title' => 'Berhasil ! ',
            'text-1' => 'Import ',
            'text-2' => 'Telah berhasil.'
        ];

        return redirect('/tamu_gambaran')->with('after_save', $after_save);

    }

    public function export(Request $request){

        $now = Carbon::now();
        $name_file = 'LaporanLelang'.$now.'-.xlsx';
        return (new LelangExport())->download($name_file);
    }    

}
