@extends('template/master')

@push('loader')
<div id="preloader">
  <div id="loader"></div>
</div>
@endpush

@push('css')
<style>
@import url('https://fonts.googleapis.com/css?family=Roboto+Slab:100,300,400,700');
@import url('https://fonts.googleapis.com/css?family=Raleway:300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i');
.cards {
    width: 100%;
    display: flex;
    display: -webkit-flex;
    justify-content: center;
    -webkit-justify-content: center;
    max-width: 820px;
    border: 1px solid #eee;
}

.card--1 .card__img, .card--1 .card__img--hover {
    background-image: url('https://images.pexels.com/photos/45202/brownie-dessert-cake-sweet-45202.jpeg?auto=compress&cs=tinysrgb&h=750&w=1260');
}

.card--2 .card__img, .card--2 .card__img--hover {
    background-image: url('https://images.pexels.com/photos/307008/pexels-photo-307008.jpeg?auto=compress&cs=tinysrgb&h=750&w=1260');
}

.card__like {
    width: 18px;
}

.card__clock {
    width: 15px;
  vertical-align: middle;
    fill: #AD7D52;
}
.card__time {
    font-size: 12px;
    color: #AD7D52;
    vertical-align: middle;
    margin-left: 5px;
}

.card__clock-info {
    float: right;
}

.card__img {
  visibility: hidden;
    background-size: cover;
    background-position: center;
    background-repeat: no-repeat;
    width: 100%;
    height: 235px;
  border-top-left-radius: 12px;
  border-top-right-radius: 12px;
  
}

.card__info-hover {
    position: absolute;
    padding: 16px;
  width: 100%;
  opacity: 0;
  top: 0;
}

.card__img--hover {
  transition: 0.2s all ease-out;
    background-size: cover;
    background-position: center;
    background-repeat: no-repeat;
    width: 100%;
  position: absolute;
    height: 235px;
  border-top-left-radius: 12px;
border-top-right-radius: 12px;
top: 0;
  
}
.card {
  margin-right: 25px;
  transition: all .4s cubic-bezier(0.175, 0.885, 0, 1);
  background-color: #fff;
  width: 100%;
  position: relative; 
  border-radius: 12px;
  overflow: hidden;
  box-shadow: 0px 13px 10px -7px rgba(0, 0, 0,0.1);
  border: 1px solid #eee;
  height: 400px;
}
.card:hover {
  box-shadow: 0px 30px 18px -8px rgba(0, 0, 0,0.1);
    transform: scale(1.10, 1.10);
}

.card__info {
z-index: 2;
  background-color: #fff;
  border-bottom-left-radius: 12px;
border-bottom-right-radius: 12px;
   padding: 16px 24px 24px 24px;
}

.card__category {
    font-family: 'Raleway', sans-serif;
    text-transform: uppercase;
    font-size: 13px;
    letter-spacing: 2px;
    font-weight: 500;
  color: #868686;
}

.card__title {
    margin-top: 5px;
    margin-bottom: 10px;
    font-family: 'Roboto Slab', serif;
}

.card__by {
    font-size: 12px;
    font-family: 'Raleway', sans-serif;
    font-weight: 500;
}

.card__author {
    font-weight: 600;
    text-decoration: none;
    color: #AD7D52;
}

.card:hover .card__img--hover {
    height: 100%;
    opacity: 0.3;
}

.card:hover .card__info {
    background-color: transparent;
    position: relative;
}

.card:hover .card__info-hover {
    opacity: 1;
}
#foto_show{
    max-width: 500;
    max-height: 400;
}

@media only screen and (max-width: 550px) {
    #foto_show{
        max-width: 400;
        max-height: 400;
    }
}
#btn_cari{
    float: right;
    position: relative;
    bottom: 35;
    left: 50;
    height: 35;
    border-radius: 5px;
}
</style>
@endpush

@section('content')
<div class="banner_inner_con">
</div>
<div class="services-breadcrumb">
<div class="inner_breadcrumb">

  <ul class="short">
  <b>  <li><a href="{{ URL('user/dashboard')}}">Pelelangan</a><span>
  </b>
  </ul>
</div>
</div>
<!--//banner_info-->
<!--/ab-->
<div class="banner_bottom">
<div class="container">
  <div class="main-panel">
    <div class="content-wrapper">
        <div class="row">
          <div class="pull-right" style="position: relative;
          right: 65;">
            <form action="{{url('/home/user/cari')}}" method="GET">
              <input type="text" class="form-control" name="cari" placeholder="Cari nama barang .." value="{{ old('cari') }}">
              <button type="btn btn-default btn-sm" type="submit"  id="btn_cari">Cari</button>
            </form>
          </div>
        </div>
        <hr>
        <br>
        <div class="row purchace-popup">
        <div class="col-12">
          <?php 
            function rupiah($angka){
              
              $hasil_rupiah = "Rp " . number_format($angka,2,',','.');
              return $hasil_rupiah;
            
            }
          ?>
          @foreach($data_lelang as $data)
            <div class="col-lg-3 col-sm-6 col-xs-12">
              <article class="card card--2">
                  <div class="card__info-hover">
                      <div class="card__clock-info">
                          <svg class="card__clock" viewBox="0 0 24 24">
                              <path d="M12,20A7,7 0 0,1 5,13A7,7 0 0,1 12,6A7,7 0 0,1 19,13A7,7 0 0,1 12,20M19.03,7.39L20.45,5.97C20,5.46 19.55,5 19.04,4.56L17.62,6C16.07,4.74 14.12,4 12,4A9,9 0 0,0 3,13A9,9 0 0,0 12,22C17,22 21,17.97 21,13C21,10.88 20.26,8.93 19.03,7.39M11,14H13V8H11M15,1H9V3H15V1Z" />
                          </svg><span class="card__time">{{$data->tgl_lelang}}</span>
                      </div>

                  </div>
                  <div class="card__img"></div>
                  <a href="#" class="card_link">
                      <div class="card__img--hover" style="background-image:url('/uploads/file/barang/{{$data->barang->foto_barang}}');"></div>
                  </a>
                  <div class="card__info">
                  <span class="card__category">{{rupiah($data->barang->harga_awal)}}</span>
                      <h3 class="card__title">{{$data->barang->nama_barang}}</h3>
                      <button type="button" class="btn btn-default btn-sm" data-toggle="modal" data-target="#detailBarang" onclick="openPenawaran({{$data}})">
                          Lakukan Penawaran
                      </button> 
                  </div>
              </article>
            </div>
          @endforeach
          
        </div>
      </div>
      <br>
      <hr>
      <br>
      <div class="pull-right">
        <p>Halaman : <b>{{ $data_lelang->currentPage() }}</b> | Jumlah Data : <b>{{ $data_lelang->total() }}</b> | Data Per Halaman : <b>{{ $data_lelang->perPage() }}</b></p>
        <div class="pull-right">{{ $data_lelang->links() }}</div>
      </div>
    </div>


    <div class="modal fade" tabindex="-1" id="detailBarang" role="dialog" data-backdrop="true">
      <div class="modal-dialog" role="document">
          <div class="modal-content">
              <!-- Modal Header -->
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true" style="color:crimson;"> &times;</span>
                  </button>
                  <center>
                      <h3 class="modal-title">Lakukan Penawaran</h3>
                  </center>
              </div>
  
              <form action="{{ URL('/pelelangan/penawaran')}}" method="POST" autocomplete="off" enctype="multipart/form-data">
                  {{ csrf_field() }}
                  <div class="modal-body" style="overflow-y: scroll;">
                      <div class="row" style="margin-top:10px;">
                          <center>
                              <img id="foto_show" ></i>
                          </center>
                      </div>
                      <hr>
                      <input type="hidden" name="id_barang" id="id_barang" />
                      <input type="hidden" name="id_lelang" id="id_lelang" />
                      <input type="hidden" name="harga_awal_hidden" id="harga_awal_hidden" />
                      <div class="row" style="margin-top:10px;">
                          <div class="col-md-12">
                              <h3 id="nama_barang"></h3>
                          </div>
                      </div>
                      <hr>
                      <div class="row">
                          <div class="col-md-12">
                              <p id="desk_barang"></p>
                          </div>
                      </div>
                      <hr>
                      <div class="row" >
                          <div class="col-md-12">
                              <h4 id="harga_awal"></h4>
                          </div>
                      </div>
                      <hr>
                      <div class="row" >
                          <div class="col-md-12">
                              <h4 id="penawaran_terakhir">Penawaran Terakhir : </h4>
                          </div>
                      </div>
                      <hr>
                      <div class="row" >
                          <div class="col-md-12">
                              <div class="form-group">
                                  <label>Input Penawaran</label>
                                  <input type="number" name="penawaran_harga" class="form-control" placeholder="Masukkan harga penawaran" id="penawaran_harga"required style="margin-top: 10;">
                              </div>
                          </div>
                      </div>
  
                  </div>
  
                  <div class="modal-footer">
                      <center>
                          <button type="submit" class="btn btn-md  btn-save btn-fill " style="background-color:#1db70c; color:white;">&nbsp;&nbsp; Submit &nbsp;&nbsp;</button>
                      </center>
                  </div>
              </form>
          </div>
      </div>
  </div>

  </div>
</div>
</div>
@endsection

@push('scripts')
<script type="text/javascript">
$(window).on("load", function () {
    $('#preloader').fadeOut(3000);
});
$("#pelelangan").addClass("active");

function formatRupiah(angka){
	var number_string = angka.toString().replace(/[^,\d]/g, '').toString(),
	split   		= number_string.split(','),
	sisa     		= split[0].length % 3,
	rupiah     		= split[0].substr(0, sisa),
	ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);
 
	// tambahkan titik jika yang di input sudah menjadi angka ribuan
	if(ribuan){
		separator = sisa ? '.' : '';
		rupiah += separator + ribuan.join('.');
	}
 
	rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
	return 'Rp.' + rupiah;
}

function openPenawaran(data){
  console.log("ini data barang", data);
  var data_barang = data['barang'];
  $("#nama_barang").text(data_barang['nama_barang']);
  $("#desk_barang").text(data_barang['deskripsi']);
  $("#harga_awal").text('Harga Awal : '+formatRupiah(data_barang['harga_awal']));
  if(data['harga_akhir'] == null){
    $("#penawaran_terakhir").text('Penawaran Terakhir : Belum ada penawaran');
    $("#harga_awal_hidden").val(data_barang['harga_awal']);
  }else{
    $("#penawaran_terakhir").text('Penawaran Terakhir : '+ formatRupiah(data['harga_akhir']) );
    $("#harga_awal_hidden").val(data['harga_akhir']);
  }
  $("#foto_show").attr("src","../../uploads/file/barang/"+data_barang['foto_barang']);
  $("#id_lelang").val(data['id_lelang']);
  $("#id_barang").val(data_barang['id_barang']);
}
</script>

@endpush
