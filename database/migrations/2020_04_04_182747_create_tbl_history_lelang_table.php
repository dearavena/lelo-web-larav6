<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblHistoryLelangTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_history_lelang', function (Blueprint $table) {
            $table->increments('id_history');
            $table->integer('id_lelang')->length(11)->nullable();
            $table->integer('id_barang')->length(11);
            $table->integer('id_user')->length(11);
            $table->integer('penawaran_harga')->length(20);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_history_lelang');
    }
}
