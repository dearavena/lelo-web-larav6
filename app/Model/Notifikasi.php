<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Notifikasi extends Model
{
    protected $table='tbl_notifikasi';

    protected $primaryKey = 'id_notif';

    public $timestamps = true;

    public $incrementing = true;

    protected $fillable = [
    'id_notifikasi','title','isi_notif'
    ];

}
