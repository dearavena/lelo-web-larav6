<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblLelangTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_lelang', function (Blueprint $table) {
            $table->increments('id_lelang');
            $table->integer('id_user')->length(11)->nullable();
            $table->integer('id_barang')->length(11);
            $table->integer('id_petugas')->length(11)->nullable();
            $table->timestamp('tgl_lelang')->nullable();
            $table->integer('harga_akhir')->length(20)->nullable();
            $table->enum('status',['nonactive','open','close']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_lelang');
    }
}
