<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $now =Carbon::now();
      DB::table('users')->insert([
            ['nama_lengkap'=>'Administrator', 'username'=>'admin123', 'password'=>bcrypt('admin123'),'telp'=>'08881331853', 'is_admin_petugas'=>1 ,'is_petugas'=>0,'created_at'=>$now],
            ['nama_lengkap'=>'Petugas Lelang', 'username'=>'petugas123', 'password'=>bcrypt('petugas123'),'telp'=>'088146338138', 'is_admin_petugas'=>1, 'is_petugas'=>1,'created_at'=>$now],
            ['nama_lengkap'=>'Masyarakat', 'username'=>'masyarakat123', 'password'=>bcrypt('masyarakat123'), 'telp'=>'0883114472284', 'is_admin_petugas'=>0, 'is_petugas'=>0,'created_at'=>$now]
        ]);
    }
}
