<html>
   <head>
      <title>LELO</title>
      <link rel="shortcut icon" href="{{asset('assets')}}/login_/images/icons/price-tags.ico">
      <!--/tags -->
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <script type="application/x-javascript">
         addEventListener("load", function() {
             setTimeout(hideURLbar, 0);
         }, false);
         
         function hideURLbar() {
             window.scrollTo(0, 1);
         }
      </script>
      <!--//tags -->
      @stack('css')
      <style>
         /* @font-face {
            font-family: 'Neusa';
            src: url({{ asset("assets/fonts/Neusa.Next/NeusaNextPro-WideRegular.otf")}}); /* IE9 Compat Modes */
         } */
         body{
            font-family: 'Neusa';
         }
         #preloader {
         position: fixed;
         top: 0;
         left: 0;
         width: 100%;
         height: 100%;
         z-index: 99999999999;
         background: white;
         }
         #loader {
         display: block;
         position: relative;
         left: 50%;
         top: 50%;
         width: 130px;
         height: 130px;
         margin: -75px 0 0 -75px;
         border-radius: 50%;
         border: 3px solid transparent;
         border-top-color: #384E6F;
         -webkit-animation: spin 2s linear infinite;
         animation: spin 2s linear infinite;
         }
         #loader:before {
         content: "";
         position: absolute;
         top: 5px;
         left: 5px;
         right: 5px;
         bottom: 5px;
         border-radius: 50%;
         border: 3px solid transparent;
         border-top-color: orange;
         -webkit-animation: spin 3s linear infinite;
         animation: spin 3s linear infinite;
         }
         #loader:after {
         content: "";
         position: absolute;
         top: 15px;
         left: 15px;
         right: 15px;
         bottom: 15px;
         border-radius: 50%;
         border: 3px solid transparent;
         border-top-color: #6D9CE4;
         -webkit-animation: spin 1.5s linear infinite;
         animation: spin 1.5s linear infinite;
         }
         @-webkit-keyframes spin {
         0% {
         -webkit-transform: rotate(0deg);
         -ms-transform: rotate(0deg);
         transform: rotate(0deg);
         }
         100% {
         -webkit-transform: rotate(360deg);
         -ms-transform: rotate(360deg);
         transform: rotate(360deg);
         }
         }
         @keyframes spin {
         0% {
         -webkit-transform: rotate(0deg);
         -ms-transform: rotate(0deg);
         transform: rotate(0deg);
         }
         100% {
         -webkit-transform: rotate(360deg);
         -ms-transform: rotate(360deg);
         transform: rotate(360deg);
         }
         }
      </style>
      <link href="{{asset('assets')}}/frontend/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
      <link href="{{asset('assets')}}/frontend/css/style.css" rel="stylesheet" type="text/css" media="all" />
      <link href="{{asset('assets')}}/frontend/css/prettyPhoto.css" rel="stylesheet" type="text/css" />
      <link href="{{asset('assets')}}/frontend/css/font-awesome.css" rel="stylesheet">
      <link href="{{asset('assets')}}/frontend/css/pe-icon-7-stroke.css" rel="stylesheet">
      <link href="{{asset('assets')}}/css/jquery.dataTables.min.css" rel="stylesheet" />
      @stack('css')
      <!-- //for bootstrap working -->
      <!-- <link href="assets/frontend/fonts.googleapis.com/css?family=Raleway:100,100i,200,300,300i,400,400i,500,500i,600,600i,700,800" rel="stylesheet">
         <link href="assets/frontend/fonts.googleapis.com/css?family=Source+Sans+Pro:300,300i,400,400i,600,600i,700" rel="stylesheet"> -->
   </head>
   <body>
      @stack('loader')
      <div class="top_header" id="home">
         <!-- Fixed navbar -->
         <nav class="navbar navbar-default navbar-fixed-top" style="opacity:0.9;">
            <div class="col-lg-12" style="margin:0px; padding:0px;">
               {{-- part alert --}} @if (Session::has('after_savee'))
               <div class="alert alert-dismissible alert-{{ Session::get('after_savee.alert') }}">
                  <i class="pe-7s-{{ Session::get('after_savee.icon') }}" style="font-size:30px; position:relative; top:8px;"></i>
                  <button type="button" class="close" data-dismiss="alert">×</button>
                  <strong>{{ Session::get('after_savee.title') }}</strong>
                  <a href="javascript:void(0)" class="alert-link">{{ Session::get('after_savee.text-1') }}</a> {{ Session::get('after_savee.text-2') }}
               </div>
               @endif {{-- end part alert --}}
            </div>
            <div class="nav_top_fx_w3ls_agileinfo">
               <div class="navbar-header">
                  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  </button>
                  <div class="logo-w3layouts-agileits">
                     <h1> <a class="navbar-brand" href="{{url('/')}}"><i class="fa fa-legal" aria-hidden="true" style="color:#333;"></i> LELO <span class="desc">Lelang Online</span></a></h1>
                  </div>
               </div>
               <div id="navbar" class="navbar-collapse collapse">
                  <div class="nav_right_top">
                     <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown" style="position: relative; bottom:5px;">
                           <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                           {{Auth::user()->nama_lengkap}}  
                           <img src="{{asset('assets')}}/frontend/images/uf1.png" width="30px" style="border-radius:50%;"></i> <span class="caret"></span></a>
                           <ul class="dropdown-menu">
                              <li><a href="#" data-toggle="modal" data-target="#profileform"><i class="glyphicon glyphicon-user"></i> Profile</a></li>
                              <li><a href="#" data-toggle="modal" data-target="#ubahpassword"><i class="glyphicon glyphicon-lock"></i> Ubah Password</a></li>
                              <li><a href="{{ url('user/logout') }}" onclick="event.preventDefault();
                                 document.getElementById('logout-form').submit();"><i class="glyphicon glyphicon-log-out"></i> Keluar</a></li>
                           </ul>
                           <form id="logout-form" action="{{ url('logout') }}" method="POST" style="display: none;">
                              {{ csrf_field() }}
                           </form>
                        </li>
                     </ul>
                     <ul class="nav navbar-nav">
                        @if(Auth::user()->is_admin_petugas == 1)
                        <li id="dashboard"><a href="{{url('/')}}">Dashboard</a></li>
                        <li id="barang"><a href="{{url('barang')}}">Barang</a></li>
                        <li id="lelang"><a href="{{url('lelang')}}">Data Lelang</a></li>
                        @else
                        <li id="pelelangan"><a href="{{url('home/user')}}">Pelelangan</a></li>
                        <li id="history"><a href="{{url('/history_user')}}">History & Notification</a></li>
                        @endif
                     </ul>
                  </div>
               </div>
               <!--/.nav-collapse -->
            </div>
         </nav>
      </div>
      <!-- banner -->
      @yield('content')
      <!--//banner -->
      </div>
      <!-- footer -->
      <div class="footer">
         <div class="footer_inner_info_w3ls_agileits">
            <div class="col-md-3 footer-left">
               <h2><a href="index.html"><img src="{{asset('assets')}}/login_/images/icons/price-tags.ico" style="width:40"></i> <span style="background:transparent;">LELO</span></a></h2>
               <p>Web atau aplikasi ini berfungsi untuk mempermudah masyarakat dalam mengajukan penawaran dalam sebuah pelelangan secara online.</p>
               <ul class="social-nav model-3d-0 footer-social social two">
                  <li>
                     <a href="#" class="facebook">
                        <div class="front"><i class="fa fa-facebook" aria-hidden="true" style="margin-top:8px;"></i></div>
                        <div class="back"><i class="fa fa-facebook" aria-hidden="true" style="margin-top:8px;"></i></div>
                     </a>
                  </li>
                  <li>
                     <a href="#" class="twitter">
                        <div class="front"><i class="fa fa-twitter" aria-hidden="true" style="margin-top:8px;"></i></div>
                        <div class="back"><i class="fa fa-twitter" aria-hidden="true" style="margin-top:8px;"></i></div>
                     </a>
                  </li>
                  <li>
                     <a href="#" class="instagram">
                        <div class="front"><i class="fa fa-instagram" aria-hidden="true" style="margin-top:8px;"></i></div>
                        <div class="back"><i class="fa fa-instagram" aria-hidden="true" style="margin-top:8px;"></i></div>
                     </a>
                  </li>
                  <li>
                     <a href="#" class="pinterest">
                        <div class="front"><i class="fa fa-linkedin" aria-hidden="true" style="margin-top:8px;"></i></div>
                        <div class="back"><i class="fa fa-linkedin" aria-hidden="true" style="margin-top:8px;"></i></div>
                     </a>
                  </li>
               </ul>
            </div>
            <div class="col-md-9 footer-right">
               <div class="sign-grds">
                  <div class="col-md-4 sign-gd">
                  </div>
                  <div class="col-md-3 sign-gd flickr-post">
                  </div>
                  <div class="col-md-5 sign-gd-two">
                     <h4>Contact <span>Information</span></h4>
                     <div class="address">
                        <div class="address-grid">
                           <div class="address-left">
                              <i class="fa fa-phone" aria-hidden="true"></i>
                           </div>
                           <div class="address-right">
                              <h6>Phone Number</h6>
                              <p>089622648692</p>
                           </div>
                           <div class="clearfix"> </div>
                        </div>
                        <div class="address-grid">
                           <div class="address-left">
                              <i class="fa fa-envelope" aria-hidden="true"></i>
                           </div>
                           <div class="address-right">
                              <h6>Email Address</h6>
                              <p>Email :<a href="mailto:orimuntashori2406@gmail.com"> dearavena@gmail.com</a></p>
                           </div>
                           <div class="clearfix"> </div>
                        </div>
                        <div class="address-grid">
                           <div class="address-left">
                              <i class="fa fa-map-marker" aria-hidden="true"></i>
                           </div>
                           <div class="address-right">
                              <h6>Location</h6>
                              <p>Jln. Moch Toha, Seke Kuda No A.27
                              </p>
                           </div>
                           <div class="clearfix"> </div>
                        </div>
                     </div>
                  </div>
                  <div class="clearfix"></div>
               </div>
            </div>
            <div class="clearfix"></div>
            <p class="copy-right">&copy 2020 LELO. All rights reserved | by <a href="#">Dea Ravena</a></p>
         </div>
      </div>
      </div>

      <div class="modal fade" tabindex="-1" id="profileform" role="dialog" data-backdrop="true" style="opacity:1;">
         <div class="modal-dialog" role="document">
            <div class="modal-content">
               <!-- Modal Header -->
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true"> &times;</span>
                  </button>
                  <h3 class="modal-title"><b>Profile</b></h3>
               </div>
               <form action="{{URL('user/changeprofile')}}" method="POST" autocomplete="off">
                  {{ csrf_field() }}
                  <div class="modal-body">
                     <div class="form-group" style="position: relative; top:10px;">
                        <center>
                           <img src="{{asset('assets')}}/img/user.png" style="max-height:200"></i>
                        </center>
                     </div> 
                     <input type="hidden" name="id_user" value="{{Auth::user()->id_user}}" required />
                     <div class="form-group" style="position: relative; top:10px;">
                        <label for="name">Nama Lengkap</label>
                        <input type="text" name="nama_lengkap" class="form-control" placeholder="Masukkan Nama Lengkap" value="{{Auth::user()->nama_lengkap}}" required>
                     </div>
                     <div class="form-group" style="position: relative; top:10px;">
                        <label for="name">Username</label>
                        <input type="text" name="username" class="form-control" placeholder="Masukkan Username" value="{{Auth::user()->username}}"required>
                     </div>
                     <div class="form-group" style="position: relative; top:10px;">
                        <label for="name">No Telepon</label>
                        <input type="text" name="telp" class="form-control" placeholder="Masukkan No Telepon" value="{{Auth::user()->telp}}" required>
                     </div>
                     <div class="form-group" style="position: relative; top:10px;">
                        <label for="name">Hak Akses</label>
                        @if(Auth::user()->is_admin_petugas)
                           @if(Auth::user()->is_petugas)
                           <input type="text" name="hakakses" class="form-control" placeholder="Masukkan Hak Akses" value="Petugas lelang" required readonly>
                           @else
                           <input type="text" name="hakakses" class="form-control" placeholder="Masukkan Hak Akses" value="Admin" required readonly>
                           @endif
                        @else
                           <input type="text" name="hakakses" class="form-control" placeholder="Masukkan Hak Akses" value="User" required readonly>
                        @endif
                     </div>
                  </div>
                  <div class="modal-footer">
                     <button type="reset" class="btn btn-raised btn-fill btn-warning "><i class="glyphicon glyphicon-refresh"> </i> Reset</button>
                     <button type="submit" class="btn btn-primary btn-save btn-fill pull-right"><i class="glyphicon glyphicon-floppy-disk"></i> Submit</button>
                  </div>
               </form>
            </div>
         </div>
      </div>

      

      {{-- ubah password  --}}
      <div class="modal fade" tabindex="-1" id="ubahpassword" role="dialog" data-backdrop="true" style="opacity:0.95;">
         <div class="modal-dialog" role="document">
            <div class="modal-content">
               <!-- Modal Header -->
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true"> &times;</span>
                  </button>
                  <h3 class="modal-title"><b>Ubah Password</b></h3>
               </div>
               <form action="{{URL('user/ubahpassword')}}" method="POST" autocomplete="off">
                  {{ csrf_field() }}
                  <div class="modal-body">
                     <div class="form-group" style="position: relative; top:10px;">
                        <label for="name">Password Baru</label>
                        <input type="password" name="passwordbaru" class="form-control" placeholder="Masukkan Password Baru" required>
                     </div>
                     <div class="form-group" style="position: relative; top:10px;">
                        <label for="name">Confirm Password</label>
                        <input type="password" name="passwordconfirm" class="form-control" placeholder="Ketik ulang password baru" required>
                     </div>
                  </div>
                  <div class="modal-footer">
                     <button type="reset" class="btn btn-raised btn-fill btn-warning "><i class="glyphicon glyphicon-refresh"> </i> Reset</button>
                     <button type="submit" class="btn btn-primary btn-save btn-fill pull-right"><i class="glyphicon glyphicon-floppy-disk"></i> Submit</button>
                  </div>
               </form>
            </div>
         </div>
      </div>


      <script src="{{asset('assets')}}/frontend/js/jquery.quicksand.js" type="text/javascript"></script>
      <script src="{{asset('assets')}}/frontend/js/jquery.prettyPhoto.js" type="text/javascript"></script>
      <script src="{{asset('assets')}}/js/jquery-1.10.2.js"></script>
      <script src="{{asset('assets')}}/js/jquery.3.2.1.min.js" type="text/javascript"></script>
      <script type="text/javascript" src="{{asset('assets')}}/js/jquery.min.js"></script>
      <script type="text/javascript" src="{{asset('assets')}}/js/dataTables/jquery.dataTables.min.js"></script>
      <script src="{{asset('assets')}}/js/bootstrap.min.js"></script>
      <!-- //jQuery-Photo-filter-lightbox-Gallery-plugin -->
      <script>
         $('ul.dropdown-menu li').hover(function() {
             $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
         }, function() {
             $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
         });
      </script>
      <!-- js -->
      @stack('scripts')
   </body>
</html>