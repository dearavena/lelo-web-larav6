<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
class IsUser
{
  public function handle($request, Closure $next)
  {
     if (Auth::user() &&  Auth::user()->is_admin_petugas == 0) {
            return $next($request);
     }

    return redirect('/');
  }
}
