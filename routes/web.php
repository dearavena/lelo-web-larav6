<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::auth();

//user
Route::get('/user/register','UserController@view_register');
Route::post('post_register','UserController@register');
//user
Route::post('/user/ubahpassword','UserController@ubahpassword');
Route::post('/user/changeprofile','UserController@update_profile');
Route::any('/notifikasi/json','UserController@jsonnotif');
//Admin--------------------------------------------------------------------
Route::group(['middleware'=>['auth','IsAdmin']], function(){

//dashboard
Route::get('/','Admin\DashboardController@index');

//barang
Route::any('/barang','Admin\BarangController@index')->name('barang');
Route::any('/barang/json','Admin\BarangController@json')->name('barang.json');
Route::get('delete_barang/{id}','Admin\BarangController@delete');
Route::post('/barang/store','Admin\BarangController@store');
Route::post('/barang/update','Admin\BarangController@update');

//datalelang
Route::any('/lelang','Admin\LelangController@index')->name('lelang');
Route::any('/lelang/json','Admin\LelangController@json')->name('lelang.json');
Route::get('delete_lelang/{id}','Admin\LelangController@delete');
Route::any('/update_status_lelang/{tipe_status}/{id_lelang}','Admin\LelangController@update_status');
Route::post('/lelang_export','Admin\LelangController@export');

//datahistory
Route::any('/history/{id_lelang}','Admin\HistoryController@index')->name('history');
Route::any('/history/json/{id_lelang}','Admin\HistoryController@json')->name('history.json');
Route::get('delete_history/{id}','Admin\HistoryController@delete');
Route::post('/history_export','Admin\HistoryController@export');


});

//batas-------------------------------------------------------------

//User----------------------------------------------------------------------
Route::group(['middleware'=>['auth','IsUser']], function(){

//user lelang
Route::get('/home/user','Masyarakat\LelangController@index');
Route::get('/home/user/cari','Masyarakat\LelangController@cari');

//history user
Route::any('/history_user','Masyarakat\HistoryController@index')->name('history_user');
Route::any('/history_user/json ','Masyarakat\HistoryController@json')->name('history_user.json');
Route::get('delete_history_user/{id}','Masyarakat\HistoryController@delete');

//Pelelangan
Route::post('/pelelangan/penawaran', 'Masyarakat\LelangController@penawaran');

});