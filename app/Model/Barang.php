<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Barang extends Model
{
    protected $table='tbl_barang';

    protected $primaryKey = 'id_barang';

    public $timestamps = true;

    public $incrementing = true;

    protected $fillable = [
    'id_barang','kode_barang','foto_barang','nama_barang',
    'deskripsi','harga_awal'
    ];


    public function lelang()
    {
        return $this->hasOne('App\Model\Lelang');
    }

    public function history()
    {
        return $this->hasMany('App\Model\HistoryLelang');
    }

}
