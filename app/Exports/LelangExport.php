<?php

namespace App\Exports;

use App\Model\HistoryLelang;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\View\View;
use App\Model\Barang;
use App\User;
use App\Model\Lelang;
use Carbon\Carbon;

class LelangExport implements FromView, ShouldAutoSize
{

    use Exportable;



    public function view(): View
    {
        $data = Lelang::with('barang','user')->get(); 
        $now = Carbon::now();
        return view('adminpetugas.laporan.DataLelang', [
            'data'=>$data,
            'now'=>$now
        ]);
    }
}
