<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    public $table='users';

    public $primaryKey="id_user";

    protected $fillable = [
        'nama_lengkap', 'username', 'password','telp','is_admin_petugas','is_petugas'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function lelang()
    {
        return $this->hasMany('App\Model\Lelang');
    }

    public function history()
    {
        return $this->hasMany('App\Model\HistoryLelang');
    }
}
