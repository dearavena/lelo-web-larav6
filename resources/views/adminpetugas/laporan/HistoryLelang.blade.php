<html>
    <head>
    </head>
    <body>
        <table>
            <tr>
                <td colspan="4">
                    <center>
                        <h3 style="text-align:center;">Laporan History Lelang {{$nama_barang}}</h3>
                    </center>
                </td>
            </tr>
            <tr>
                <th>No</th>
                <th>Nama Barang</th>
                <th>Nama Penawar</th>
                <th>Penawaran Harga</th>
            </tr>
            @foreach($data as $index => $datanew)
            <tr>
                <td>{{ ($index+1) }}</td>
                <td>{{ $datanew->barang->nama_barang }}</td>
                <td>{{ $datanew->user->nama_lengkap }}</td>
                <td>{{ $datanew->penawaran_harga }}</td>
            </tr>
            @endforeach
        </table>
    </body>
</html>