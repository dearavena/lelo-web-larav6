@extends('template/master')

@push('loader')
<div id="preloader">
  <div id="loader"></div>
</div>
@endpush

@push('css')
<style>
@import url('https://fonts.googleapis.com/css?family=Roboto+Slab:100,300,400,700');
@import url('https://fonts.googleapis.com/css?family=Raleway:300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i');
label {
    margin-top: 10 !important;
}
.dataTables_wrapper .dataTables_filter {
    float: right;
    text-align: right;
    margin-bottom: 10 !important;
}

#foto_show{
    max-width: 500;
    max-height: 400;
}

@media only screen and (max-width: 550px) {
    #foto_show{
        max-width: 400;
        max-height: 400;
    }
}

@media only screen and (min-width : 1250px){
.main-panel{
    width: 110%;
    position: relative;
    right: 4%;
}
}
</style>
@endpush

@section('content')
<div class="banner_inner_con">
</div>
<div class="services-breadcrumb">
<div class="inner_breadcrumb">

  <ul class="short">
  <b>  <li><a href="{{ URL('barang')}}">Barang</a><span>
  </b>
  </ul>
</div>
</div>
<!--//banner_info-->
<!--/ab-->
<div class="banner_bottom">
    <div class="container">
        <div class="main-panel">
        <div class="content-wrapper">
            <div class="row purchace-popup">
            <div class="col-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                <span style="font-size:23px;">Data Barang</span>
                </div><br>
                {{-- part alert --}}
                    @if (Session::has('after_save'))
                            <div class="col-md-12" style="margin-top:20px;">
                                <div class="alert alert-xs alert-dismissible alert-{{ Session::get('after_save.alert') }}">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <strong>{{ Session::get('after_save.title') }}</strong>
                                <a href="javascript:void(0)" class="alert-link">{{ Session::get('after_save.text-1') }}</a> {{ Session::get('after_save.text-2') }}
                                </div>
                            </div>
                    @endif
                {{-- end part alert --}}
            <button type="button" class="btn btn-md btn-primary pull-left " style="margin-left: 15;" data-toggle="modal" data-target="#addForm" id="tambah_form"><i class="fa fa-plus"></i> Tambah</button>
            <div class="panel-body">
                {{-- <div class="table-responsive"> --}}
                <table  class="table table-striped table-bordered table-primary table-md table-hover " id="tbl-barang" style="width:100%;">
                    <thead>
                        <tr>
                        <td>No</td>
                        <td>Kode</td>
                        <td>Nama Barang</td>
                        <td>Deskripsi</td>
                        <td>Harga Awal</td>
                        <td>Ditambahkan Pada</td>
                        <td style="width:15%;">Action</td>
                        </tr>
                    </thead>
                </table>
                {{-- </div> --}}
                </div>
                </div>
                </div>
            </div>
            </div>
    </div>
</div>

<!-- Add Modal Add Barang-->
<div class="modal fade" tabindex="-1" id="addForm" role="dialog" data-backdrop="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" style="color:crimson;"> &times;</span>
                </button>
                <center>
                    <h3 class="modal-title">Tambah Barang</h3>
                </center>
            </div>

            <form action="{{ URL('/barang/store')}}" method="POST" autocomplete="off" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="modal-body" style="overflow-y: scroll;">
                    <div class="row" style="margin-top:10px;">
                        <div class="col-sm-12">
                            <label for="name">Kode Barang </label>
                            <input type="text" name="kode_barang" class="form-control" id="kode_barang_new" placeholder="Masukkan kode barang" required readonly>
                        </div>
                    </div>

                    <div class="row" style="margin-top:10px;">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Nama Barang</label>
                                <input type="text" name="nama_barang" class="form-control" placeholder="Masukkan nama barang" required>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Foto Barang</label>
                                <input type="file" name="foto_barang" class="form-control "   required onchange="readURL(this)"  id="gambar">
                                <img style="max-height:150px; margin-top:15px;" id="foto"></i>
                            </div>
                        </div>
                    </div>

                    <div class="row" >
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Deskripsi Barang</label>
                                <textarea  name="deskripsi" class="form-control" required>
                                </textarea>
                            </div>
                        </div>
                    </div>

                    <div class="row" >
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Harga Awal</label>
                                <input type="number" name="harga_awal" class="form-control" placeholder="Masukkan harga awal" required>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="modal-footer">
                    <center>
                        <button type="submit" class="btn btn-md  btn-save btn-fill " style="background-color:#1db70c; color:white;">&nbsp;&nbsp; Save &nbsp;&nbsp;</button>
                    </center>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Edit Modal Barang-->
<div class="modal fade" tabindex="-1" id="editForm" role="dialog" data-backdrop="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" style="color:crimson;"> &times;</span>
                </button>
                <center>
                    <h3 class="modal-title">Edit Barang</h3></center>
            </div>

            <form action="{{ URL('/barang/update')}}" method="POST" autocomplete="off" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="modal-body" style="overflow-y: scroll;">
                    <input type="hidden" name="id_barang" id="id_barang" required/>

                    <div class="row" style="margin-top:10px;">
                        <div class="col-sm-12">
                            <label for="name">Kode Barang </label>
                            <input type="text" name="kode_barang" id="kode_barang" class="form-control" placeholder="Masukkan kode barang" required>
                        </div>
                    </div>

                    <div class="row" style="margin-top:10px;">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Nama Barang</label>
                                <input type="text" name="nama_barang" id="nama_barang" class="form-control" placeholder="Masukkan nama barang" required>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Foto Barang</label>
                                <input type="file" name="foto_barang" class="form-control "   onchange="readURL2(this)"  id="gambar2">
                                <img style="max-height:150px; margin-top:15px;" id="foto2"></i>
                            </div>
                        </div>
                    </div>

                    <div class="row" >
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Deskripsi Barang</label>
                                <textarea  name="deskripsi" id="deskripsi" class="form-control" value="" required>
                                </textarea>
                            </div>
                        </div>
                    </div>

                    <div class="row" >
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Harga Awal</label>
                                <input type="number" name="harga_awal" id="harga_awal" class="form-control" placeholder="Masukkan harga awal" required>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="modal-footer">
                    <center>
                        <button type="submit" class="btn btn-md  btn-save btn-fill " style="background-color:#1db70c; color:white;">&nbsp;&nbsp; Submit &nbsp;&nbsp;</button>
                    </center>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Show foto-->
<div class="modal fade" tabindex="-1" id="showPhotos" role="dialog" data-backdrop="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" style="color:crimson;"> &times;</span>
                </button>
                <center>
                    <h3 class="modal-title">Foto Barang</h3></center>
            </div>
            <div class="modal-body" style="height: 420;">
                <center>
                        <center> 
                            <img id="foto_show" ></i>
                        </center>
                </center>
            </div>
        </div>
    </div>
</div>

@endsection

@push('scripts')
<script type="text/javascript">
$(window).on("load", function () {
    $('#preloader').fadeOut(3000);
});
$("#barang").addClass("active");

var t_barang;

$(function() {
    t_barang = $('#tbl-barang').DataTable({
        pagingType:"full_numbers",
        scrollX:true,
        processing: true,
          "language": {
        processing: "<img src='{{asset('assets/img/2 (2).gif')}}'> "},
        serverSide: true,
        ajax:{
          url:'{{ route("barang.json") }}',
      },
        columns: [
            { data: null, orderable: false},
            { data: 'kode_barang', name: 'kode_barang' },
            { data: 'nama_barang', name: 'nama_barang' },
            { data: 'deskripsi', name: 'deskripsi' },
            { data: 'harga_awal', name: 'harga_awal' , render: function(data,type,row){
                return formatRupiah(row.harga_awal,'');
            }},
            { data: 'created_at', name: 'created_at' },
            { data: 'action', orderable:false, searchable:false }
          ],
          dom: 'B<"toolbar">ifrtlp',
          "rowCallback": function (nRow, aData, iDisplayIndex) {
           var oSettings = this.fnSettings ();
           $("td:first", nRow).html(oSettings._iDisplayStart+iDisplayIndex +1);
           return nRow;
         }
 } );

 $(document).on('click', '.delete', function(){
       if(confirm("Are you sure you want to Delete this data?"))
       {
        alert('Record deleted successfully.'); window.location.href='/income';
       }
       else
       {
           return false;
       }
   });
   
   $('#tbl-barang tbody').on( 'click', '#show_edit', function () {
        var data = t_barang.row( $(this).parents('tr') ).data();
        console.log(data);
        $("#kode_barang").val(data['kode_barang']);
        $("#id_barang").val(data['id_barang']);
        $("#nama_barang").val(data['nama_barang']);
        $("textarea#deskripsi").val(data['deskripsi']) ;
        $("#foto2").attr("src","../../uploads/file/barang/"+data['foto_barang']);
        $("#harga_awal").val(data['harga_awal']) ;
   });

   $('#tbl-barang tbody').on( 'click', '#show-foto', function () {
        var data = t_barang.row( $(this).parents('tr') ).data();
        console.log(data);
        $("#foto_show").attr("src","../../uploads/file/barang/"+data['foto_barang']);
   });

});

function formatRupiah(angka, prefix){
	var number_string = angka.replace(/[^,\d]/g, '').toString(),
	split   		= number_string.split(','),
	sisa     		= split[0].length % 3,
	rupiah     		= split[0].substr(0, sisa),
	ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);
 
	// tambahkan titik jika yang di input sudah menjadi angka ribuan
	if(ribuan){
		separator = sisa ? '.' : '';
		rupiah += separator + ribuan.join('.');
	}
 
	rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
	return prefix == undefined ? rupiah : (rupiah ? 'Rp.' + rupiah : '');
}

$('#tambah_form').on( 'click',function () {

    $('#kode_barang_new').val(Math.floor(100000000 + Math.random() * 900000000));

});

function readURL(input) {
        var file = document.getElementById('gambar').files[0];

        if(file && file.size < 5000000) { //1mb size file validation
            var file = document.querySelector("#gambar");
            if ( /\.(jpe?g|png|gif)$/i.test(file.files[0].name) === false )
            {
            alert("Error : File type must be ( jpg,jpeg,png,gif ) !");
            input.value = null; // Clear the field.
            }else{
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#foto')
                            .attr('src', e.target.result);
                    };

                    reader.readAsDataURL(input.files[0]);
            }
            }

        } else {

            alert("Error : File size over 5mb !"); // Do your thing to handle the error.
            input.value = null; // Clear the field.
        }
}

function readURL2(input) {
        var file = document.getElementById('gambar2').files[0];

        if(file && file.size < 5000000) { //1mb size file validation
            var file = document.querySelector("#gambar2");
            if ( /\.(jpe?g|png|gif)$/i.test(file.files[0].name) === false )
            {
            alert("Error : File type must be ( jpg,jpeg,png,gif ) !");
            input.value = null; // Clear the field.
            }else{
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#foto2')
                            .attr('src', e.target.result);
                    };

                    reader.readAsDataURL(input.files[0]);
            }
            }

        } else {

            alert("Error : File size over 5mb !"); // Do your thing to handle the error.
            input.value = null; // Clear the field.
        }
}


</script>

@endpush
