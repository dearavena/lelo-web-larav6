<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Lelang extends Model
{
    protected $table='tbl_lelang';

    protected $primaryKey = 'id_lelang';

    public $timestamps = false;

    public $incrementing = true;

    protected $fillable = [
    'id_lelang','id_user','id_barang','id_petugas',
    'tgl_lelang','harga_akhir','status'
    ];


    public function barang()
    {
        return $this->belongsTo('App\Model\Barang', 'id_barang');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'id_user');
    }
    
    public function history()
    {
        return $this->hasMany('App\Model\HistoryLelang');
    }
}
