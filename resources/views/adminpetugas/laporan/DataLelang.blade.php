<html>
    <head>
    </head>
    <body>
        <table>
            <tr>
                <td colspan="5">
                    <center>
                        <h3 style="text-align:center;">Laporan Data Lelang {{$now}}</h3>
                    </center>
                </td>
            </tr>
            <tr>
                <th>No</th>
                <th>Nama Barang</th>
                <th>Harga Awal</th>
                <th>Nama Pemenang</th>
                <th>Penawaran Terakhir</th>
            </tr>
            @foreach($data as $index => $datanew)
            <tr>
                <td>{{ ($index+1) }}</td>
                <td>{{ $datanew->barang->nama_barang }}</td>
                <td>{{ $datanew->barang->harga_awal }}</td>
                @if($datanew->user == null)
                    @if($datanew->status == 'nonactive')
                        <td>Belum Ada Penawaran</td>
                    @else
                        <td>Pemenang Muncul Setelah Pelelangan ditutup</td>
                    @endif
                @else
                    <td>{{$datanew->user->nama_lengkap}}</td>
                @endif
                <td>
                    @if($datanew->harga_akhir == null)
                        Belum Ada Penawaran
                    @else
                        {{$datanew->harga_akhir}}
                    @endif
                </td>
            </tr>
            @endforeach
        </table>
    </body>
</html>