<?php

namespace App\Http\Controllers\Masyarakat;

use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Models\Event;
use Carbon\Carbon;
use DataTables;
use Auth;
use PDF;
use App\Model\Barang;
use App\User;
Use App\Model\Lelang;
use App\Model\HistoryLelang;
use Excel;
use App\Exports\HistoryExport;
use Illuminate\Http\Request;

class HistoryController extends Controller
{
    public function index(){

        return view('masyarakat.history.history');
 
    }
    public function json(){
        $data = HistoryLelang::with('barang','user')->where('id_user','=',Auth::user()->id_user)->orderBy('created_at','DESC')->get();
        return Datatables::of($data)
        ->addColumn('action', function($data){
                return '
                <a href="/delete_history_user/'.$data->id_history.'" class="delete btn btn-small btn-danger"  class="delete" style="float:left !important; margin-right:5">Cancel</a>
                ';
        })
        ->make(true);
    }

    public function delete($id){
        $res= HistoryLelang::where('id_history',$id)->delete();
        $after_save = [
                'alert' => 'success',
                'icon' => 'check',
                'title' => 'Berhasil ! ',
                'text-1' => 'Data History Lelang ',
                'text-2' => 'Telah dihapus.'
            ];
        return redirect()->back()->with('after_save', $after_save);
    
    }
    

}
    