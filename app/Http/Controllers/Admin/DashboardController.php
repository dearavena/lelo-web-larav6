<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Models\Event;
use Carbon\Carbon;
use DataTables;
use Auth;
use PDF;
use App\Model\Barang;
use App\User;
Use App\Model\Lelang;
use App\Model\HistoryLelang;
use Excel;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index(){
        $count_barang = Barang::all()->count();
        $count_user= User::all()->where('is_admin_petugas','=',false)->count();
        $count_lelang = Lelang::all()->where('status','=','open')->count();
        return view('adminpetugas.dashboard.dashboard', ['count_barang'=>$count_barang, 'count_user'=>$count_user, 'count_lelang'=>$count_lelang]);
    }
}
